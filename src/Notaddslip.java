import static org.junit.jupiter.api.Assertions.*;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.TakesScreenshot;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.commons.io.FileUtils;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
class Notaddslip {

    @Test
    public void test_Reservation() throws Exception {
        System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
        
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        String test_Date = formatter.format(date); // วันที่การทดสอบ
		String testCase_ID = "SO 2.0.002"; // รหัสการทดสอบ
		String test_Title = "Notaddslip"; // หัวข้อการทดสอบ
		String tester_Name = "Supatta Yoophiphat"; // ชื่อผู้ทดสอบ
		int sheet_Number = 4; // ลำดับชีต
		String excel_Path = "C:/Users/Aspire V/eclipse-workspace/TUC01_Reservation/TestIVanProject.xlsx";
		FileInputStream fs_excel_path = new FileInputStream(excel_Path);
		System.out.println("Connect with Excel is complete.");
		
		XSSFWorkbook workbook = new XSSFWorkbook(fs_excel_path);
		int sheets = sheet_Number-1;
		XSSFSheet sheet = workbook.getSheetAt(sheets);
		int row = sheet.getLastRowNum()+1;
		for(int i = 1; i < row; i++) {
	        ChromeOptions options = new ChromeOptions();
	        options.addArguments("--start-fullscreen");
			String Test_ID = sheet.getRow(i).getCell(0).toString();
			
			if (Test_ID.equals(testCase_ID)) {
				String email = sheet.getRow(i).getCell(2).toString();
				String password = sheet.getRow(i).getCell(3).toString();
				WebDriver driver = new ChromeDriver(options);
				System.out.println("Connect with ChromeDriver is complete.");
		        driver.get("https://www.ivango.lnw.mn/");
		        System.out.println("Open website is complete.");
		        

		        driver.findElement(By.xpath("/html/body/nav/div/div/center/a[1]")).click();
		        driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/form/div[1]/div/input")).sendKeys(email);
		        driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/form/div[2]/div/input[1]")).sendKeys(password);
		        driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/form/center/button")).click();
		        driver.findElement(By.xpath("/html/body/div[5]/div/div[3]/div/button")).click();
		        driver.findElement(By.xpath("/html/body/nav/div/div/ul/li[2]/a")).click();
		        
		        Select start = new Select(driver.findElement(By.id("select-start-form")));
		        start.selectByVisibleText("นครปฐม (มาลัยแมน)");
		        
		        Select end = new Select(driver.findElement(By.id("select-end-form")));
		        end.selectByVisibleText("บ้านโป่ง");
		        
		        driver.findElement(By.xpath("/html/body/div[1]/div[2]/form/div[1]/div/div[3]/input")).sendKeys("10202229");
		        
		        driver.findElement(By.xpath("/html/body/div[1]/div[2]/form/div[1]/div/div[4]/div/select")).click();
		        Select settime = new Select(driver.findElement(By.xpath("/html/body/div[1]/div[2]/form/div[1]/div/div[4]/div/select")));
		        settime.selectByVisibleText("15.30");
		        Thread.sleep(1000);
		       
		        Select setnum = new Select(driver.findElement(By.xpath("/html/body/div[1]/div[2]/form/div[2]/div[2]/div[1]/div/div[2]/div/select")));
		        setnum.selectByVisibleText("1");
		        Thread.sleep(2000);
		        
		        JavascriptExecutor js = (JavascriptExecutor) driver;
		        js.executeScript("window.scrollBy(0,500)");
		        Thread.sleep(2000);
		        driver.findElement(By.xpath("/html/body/div[1]/center/button")).click();//ปุ่มยืนยันการจอง1
		        Thread.sleep(2000);
		       
		      
		        String ActualTitle = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/center/h2")).getText();
		        String ExpectedTitle = "ยืนยันสถานะการจองตั๋ว";

		        if (ActualTitle.equals(ExpectedTitle)) {
//					Cell cell_3 = rows.createCell(6);
//					cell_3.setCellValue("Passed");
		            System.out.println("Step 1 : Pass_reservation1");
				} else {
//					Cell cell_3 = rows.createCell(6);
//					cell_3.setCellValue("Failed");
		            System.out.println("Step 1 : Fail_reservation1");
				}
		        
		        Thread.sleep(1000);
		        
		       
		 // ยืนยันสถานะการจองตั๋ว1
		        
		        JavascriptExecutor js1 = (JavascriptExecutor) driver;
		        js1.executeScript("window.scrollBy(0,500)");
		        JavascriptExecutor js4 = (JavascriptExecutor) driver;
		        js4.executeScript("window.scrollBy(0,500)");
		        Thread.sleep(1000);       
		        driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/center/div[3]/div[2]/div[3]/a[1]")).click();
		        
//		      Alertดำเนินการต่อ
		        Thread.sleep(4000);
		        String ActualTitle1 = driver.findElement(By.xpath("/html/body/div[3]/div/div[2]")).getText();
		        String ExpectedTitle1 = "ยืนยันการจองสำเร็จ";
		        if (ActualTitle1.equals(ExpectedTitle1)) {
//					Cell cell_3 = rows.createCell(6);
//					cell_3.setCellValue("Passed");
		            System.out.println("Step 2 : Pass_reservation1");
				} else {
//					Cell cell_3 = rows.createCell(6);
//					cell_3.setCellValue("Failed");
		            System.out.println("Step 2 : Fail_reservation1");
				}
		        Thread.sleep(1000);
		         
		        
		        driver.findElement(By.xpath("/html/body/div[3]/div/div[3]/div/button")).click();
		        JavascriptExecutor js2 = (JavascriptExecutor) driver;
		        js2.executeScript("window.scrollBy(0,500)");
		        Thread.sleep(1000);
		        driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/form/table[4]/tbody[1]/tr/td/div/div[1]/div/input")).click();
		        
//				ใส่สลิป
		        JavascriptExecutor js3 = (JavascriptExecutor) driver;
		        js3.executeScript("window.scrollBy(0,500)");
		        Thread.sleep(1000);
//		        driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/form/table[4]/tbody[2]/tr/td[1]/div/input[2]")).click();
		        
//		        WebElement browse = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/form/table[4]/tbody[2]/tr/td[1]/div/input[2]"));
//		        browse.sendKeys("C:\\Users\\Aspire V\\Downloads\\pic\\ดาวน์โหลด.jpg");
		        
//				ปุ่มชำระเงิน
		        driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/form/center/button")).click();
		        
//		        Thread.sleep(4000);
//		        String ActualTitle2 = driver.findElement(By.xpath("/html/body/div[3]/div/div[1]")).getText();
//		        String ExpectedTitle2 = "ชำระเงินเรียบร้อยแล้ว!";
//		        if (ActualTitle2.equals(ExpectedTitle2)) {
////					Cell cell_3 = rows.createCell(6);
////					cell_3.setCellValue("Passed");
//		            System.out.println("Step 3 : Pass_reservation1");
//				} else {
////					Cell cell_3 = rows.createCell(6);
////					cell_3.setCellValue("Failed");
//		            System.out.println("Step 3 : Fail_reservation1");
//				}
//		        Thread.sleep(3000);
		        
		        
		        
		        Thread.sleep(3000);
		        String ActualTitle3 = driver.findElement(By.xpath("/html/body/div[3]/div/div[2]")).getText();
		        String ExpectedTitle3 = "เกิดข้อผิดพลาด";
		        Row rows = sheet.getRow(i);
		        Cell cell_1 = rows.createCell(1);
				cell_1.setCellValue(test_Title);
				Cell cell_2 = rows.createCell(5);
				cell_2.setCellValue(ActualTitle3);
		        if (ActualTitle3.equals(ExpectedTitle3)) {
					Cell cell_3 = rows.createCell(6);
					cell_3.setCellValue("Passed");
		            System.out.println("Step 3 : Notaddslip Pass");
				} else {
					Cell cell_3 = rows.createCell(6);
					cell_3.setCellValue("Failed");
		            System.out.println("Step 3 : Notaddslip Fail");
				}
		        Cell cell_4 = rows.createCell(7);
				cell_4.setCellValue(test_Date);
				Cell cell_5 = rows.createCell(8);
				cell_5.setCellValue(tester_Name);

				FileOutputStream fos = new FileOutputStream(excel_Path);
				workbook.write(fos);
				System.out.println("Add value to Excel is complete.");
		        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		        try {
		            FileUtils.copyFile(screenshot, new File("C:/Users/Aspire V/eclipse-workspace/TUC01_Reservation/images_test/"+testCase_ID+".png"));
		            System.out.println("Upload image file is complete.");
		        } catch (IOException e) {
		            System.out.println(e.getMessage());
		        }
		        driver.close(); 
			}else {
				System.out.println("skip for loop ("+ i +") is complete.");
			}
     
        
       
		}
		System.out.println("==================================================");
        System.out.println("Test "+test_Title+" ("+testCase_ID+")");
        System.out.println("By "+tester_Name);
        System.out.println("On "+test_Date);
        System.out.println("Test Successfully.");
		System.out.println("==================================================");
    }
}