import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.commons.io.FileUtils;

class Check_status_reservation {

	@Test
	public void test_reservation_incomplete() throws Exception {
        System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
        
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
	    Date date = new Date();  
		String test_Date = formatter.format(date); // วันที่การทดสอบ
		String testCase_ID = "SO 1.0.001.1"; // รหัสการทดสอบ
		String test_Title = "Check_status_reservation"; // หัวข้อการทดสอบ
		String tester_Name = "Bubpachol Suwanwisoot"; // ชื่อผู้ทดสอบ
		int sheet_Number = 4; // ลำดับชีต
        
		String excel_Path = "C:/Users/Aspire V/eclipse-workspace/TUC01_Reservation/TestIVanProject.xlsx";
		FileInputStream fs_excel_path = new FileInputStream(excel_Path);
		System.out.println("Connect with Excel is complete.");

		XSSFWorkbook workbook = new XSSFWorkbook(fs_excel_path);
		int sheets = sheet_Number-1;
		XSSFSheet sheet = workbook.getSheetAt(sheets);
		int row = sheet.getLastRowNum()+1;

		for(int i = 1; i < row; i++) {
		
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--start-fullscreen");

        String Test_ID = sheet.getRow(i).getCell(0).toString();

        if (Test_ID.equals(testCase_ID)) {
			String email = sheet.getRow(i).getCell(2).toString();
			String password = sheet.getRow(i).getCell(3).toString();
		    WebDriver driver = new ChromeDriver(options);
			System.out.println("Connect with ChromeDriver is complete.");

			
			
//		WebDriver driver = new ChromeDriver(options);
		//Login
        driver.get("https://www.ivango.lnw.mn/");
        driver.findElement(By.xpath("/html/body/nav/div/div/center/a[1]")).click();
        driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/form/div[1]/div/input")).sendKeys(email);
        driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/form/div[2]/div/input[1]")).sendKeys(password);
        driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/form/center/button")).click();
        driver.findElement(By.xpath("/html/body/div[5]/div/div[3]/div/button")).click();
        driver.findElement(By.xpath("/html/body/nav/div/div/ul/li[5]/a")).click();
        
        String ActualTitle = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/center/h2")).getText();
        String ExpectedTitle = "ตรวจสอบสถานะการจอง";

		if (ActualTitle.equals(ExpectedTitle)) {
            System.out.println("Step 1 : Login passed.");
		} else {
            System.out.println("Step 1 : Login failed!");
		}
		
		driver.get("https://www.ivango.lnw.mn/checking");
		driver.navigate().refresh();
		Thread.sleep(2000);
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,500)");
        Thread.sleep(1000);
        
        driver.findElement(By.xpath("/html/body/center/a")).click();//ปุ่มรับตั๋ว
        Thread.sleep(2000);
//        driver.findElement(By.xpath("/html/body/div[3]/div/div[4]/div/button")).click();//ปุ่มรับทราบ ระบบจะทำการแจ้งเตือนไปยังเบอร์โทรศัพท์หมายเลข 0624074699 ล่วงหน้า 10 นาที ก่อนที่รถจะเทียบท่าสถานีนครปฐม (มาลัยแมน)
//        Thread.sleep(1000);
        JavascriptExecutor js1 = (JavascriptExecutor) driver;
        js1.executeScript("window.scrollBy(0,500)");
        // String actualString = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/center/h2")).getText();
        // Assert.assertTrue(actualString.contains("ยืนยันสถานะการจองตั๋ว"));
        
        
        String ActualTitle1 = driver.findElement(By.xpath("/html/body/div[1]/center/div/span/h1")).getText();
        String ExpectedTitle1 = sheet.getRow(i).getCell(4).toString();
        Row rows = sheet.getRow(i);
		Cell cell_1 = rows.createCell(1);
		cell_1.setCellValue(test_Title);
		Cell cell_2 = rows.createCell(5);
		cell_2.setCellValue(ActualTitle1);
        if (ActualTitle1.equals(ExpectedTitle1)) {
        	Cell cell_3 = rows.createCell(6);
			cell_3.setCellValue("Passed");
            System.out.println("Step 1 : Showstatus passed.");
            
        } else {
        	Cell cell_3 = rows.createCell(6);
			cell_3.setCellValue("Fail");
            System.out.println("Step 1 : Showstatus failed!");
        }
        Cell cell_4 = rows.createCell(7);
		cell_4.setCellValue(test_Date);
		Cell cell_5 = rows.createCell(8);
		cell_5.setCellValue(tester_Name);
		FileOutputStream fos = new FileOutputStream(excel_Path);
		workbook.write(fos);
		System.out.println("Add value to Excel is complete.");
		Thread.sleep(3000);
		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(screenshot, new File("C:/Users/Aspire V/eclipse-workspace/TUC01_Reservation/images_test/"+testCase_ID+".png"));
            System.out.println("Upload image file is complete.");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        driver.close();

		} else {
			System.out.println("skip for loop ("+ i +") is complete.");
		}
		}
		System.out.println("==================================================");
        System.out.println("Test "+test_Title+" ("+testCase_ID+")");
        System.out.println("By "+tester_Name);
        System.out.println("On "+test_Date);
        System.out.println("Test Successfully.");
		System.out.println("==================================================");

	}
	
}
