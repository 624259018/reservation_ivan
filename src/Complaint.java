import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
class Complaint {

    @Test
    void test() throws Exception {
    	System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");

	    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
	    Date date = new Date();  
		String test_Date = formatter.format(date); // วันที่การทดสอบ
		String testCase_ID = "TUC10.001"; // รหัสการทดสอบ
		String test_Title = "Complaint"; // หัวข้อการทดสอบ
		String tester_Name = "Sirichai Bootpeng"; // ชื่อผู้ทดสอบ
		int sheet_Number = 3; // ลำดับชีต
		String excel_Path = "C:/Users/Aspire V/eclipse-workspace/TUC01_Reservation/TestIVanProject.xlsx";
		FileInputStream fs_excel_path = new FileInputStream(excel_Path);
		System.out.println("Connect with Excel is complete.");
		
		XSSFWorkbook workbook = new XSSFWorkbook(fs_excel_path);
		int sheets = sheet_Number-1;
		XSSFSheet sheet = workbook.getSheetAt(sheets);
		int row = sheet.getLastRowNum()+1;

		for(int i = 1; i < row; i++) {
		ChromeOptions options = new ChromeOptions();
	    options.addArguments("--start-fullscreen");
		String Test_ID = sheet.getRow(i).getCell(0).toString();

			if (Test_ID.equals(testCase_ID)) {
				String email = sheet.getRow(i).getCell(2).toString();
				String password = sheet.getRow(i).getCell(3).toString();
				String report_title = sheet.getRow(i).getCell(4).toString();
				String type = sheet.getRow(i).getCell(5).toString();
				String text = sheet.getRow(i).getCell(6).toString();
				WebDriver driver = new ChromeDriver(options);
				System.out.println("Connect with ChromeDriver is complete.");
		        driver.get("https://www.ivango.lnw.mn/");
		        System.out.println("Open website is complete.");

        driver.findElement(By.xpath("/html/body/nav/div/div/center/a[1]")).click();
        driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/form/div[1]/div/input")).sendKeys(email);
        driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/form/div[2]/div/input[1]")).sendKeys(password);
        driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/form/center/button")).click();
        driver.findElement(By.xpath("/html/body/div[5]/div/div[3]/div/button")).click();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        
        js.executeScript("window.scrollBy(0,5000)");
        Thread.sleep(1000);
        
        driver.findElement(By.xpath("/html/body/div[4]/div[1]/div[1]/div[1]/a")).click();
        driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/form/div/div[2]/div/input")).sendKeys(report_title);
        //driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/form/div/div[4]/div/select/option[5]")).click();
        Select end = new Select(driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/form/div/div[4]/div/select")));
        end.selectByVisibleText(type);

        driver.findElement(By.id("comment")).sendKeys(text);
        driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/form/center/button")).click();
        
        String ActualTitle = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div")).getText();
        String ExpectedTitle = sheet.getRow(i).getCell(7).toString();
		Row rows = sheet.getRow(i);
		Cell cell_1 = rows.createCell(1);
		cell_1.setCellValue(test_Title);
		Cell cell_2 = rows.createCell(8);
		cell_2.setCellValue(ActualTitle);
		if (ActualTitle.equals(ExpectedTitle)) {
			Cell cell_3 = rows.createCell(9);
			cell_3.setCellValue("Passed");
            System.out.println("Step 1 : Report passed.");
		} else {
			Cell cell_3 = rows.createCell(9);
			cell_3.setCellValue("Failed");
            System.out.println("Step 1 : Report failed!");
		}
		Cell cell_4 = rows.createCell(10);
		cell_4.setCellValue(test_Date);
		Cell cell_5 = rows.createCell(11);
		cell_5.setCellValue(tester_Name);
		FileOutputStream fos = new FileOutputStream(excel_Path);
		workbook.write(fos);
		System.out.println("Add value to Excel is complete.");

		Thread.sleep(3000);
		File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            FileUtils.copyFile(screenshot, new File("C:/Users/Aspire V/eclipse-workspace/TUC01_Reservation/images_test/"+testCase_ID+".png"));
            System.out.println("Upload image file is complete.");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }  
        Thread.sleep(3000);
        driver.close(); // Close driver
		} else {
		System.out.println("skip for loop ("+ i +") is complete.");
	}       

	    
	}	
       
		System.out.println("==================================================");
        System.out.println("Test "+test_Title+" ("+testCase_ID+")");
        System.out.println("By "+tester_Name);
        System.out.println("On "+test_Date);
        System.out.println("Test Successfully.");
		System.out.println("==================================================");
    
        
    }

}
