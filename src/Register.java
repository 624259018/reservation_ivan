import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

class Register {

    @Test
    public void tuc01() throws Exception {
        System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");

	    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
	    Date date = new Date();  
		String test_Date = formatter.format(date); // วันที่การทดสอบ
		String test_Title = "Register";
		String testCase_ID = "TUC01"; // รหัสการทดสอบ
		String tester_Name = "Saran Wringsimma"; // ชื่อผู้ทดสอบ
		int sheet_Number = 1; // ลำดับชีต
		
		String excel_Path = "C:/Users/Aspire V/eclipse-workspace/TUC01_Reservation/TestIVanProject.xlsx";
		FileInputStream fs_excel_path = new FileInputStream(excel_Path);
		System.out.println("Connect with Excel is complete.");
		
		XSSFWorkbook workbook = new XSSFWorkbook(fs_excel_path);
		int sheets = sheet_Number-1;
		XSSFSheet sheet = workbook.getSheetAt(sheets);
		int row = sheet.getLastRowNum()+1;

		for(int i = 1; i < row; i++) {
	        ChromeOptions options = new ChromeOptions();
	        options.addArguments("--start-fullscreen");
			WebDriver driver = new ChromeDriver(options);
			System.out.println("Connect with ChromeDriver is complete.");
	        driver.get("https://www.ivango.lnw.mn/");
	        System.out.println("Open website is complete.");
			String Test_ID = sheet.getRow(i).getCell(0).toString();
			if (Test_ID.equals("TUC01_01")) {
				System.out.println("Runing... "+Test_ID);
				String firstname = sheet.getRow(i).getCell(2).toString();
				String lastname = sheet.getRow(i).getCell(3).toString();
				String phone = sheet.getRow(i).getCell(4).toString();
				String email = sheet.getRow(i).getCell(5).toString();
				String password = sheet.getRow(i).getCell(6).toString();
				String confirmpassword = sheet.getRow(i).getCell(7).toString();
		        driver.findElement(By.xpath("/html/body/nav/div/div/center/a[2]")).click();
		        driver.findElement(By.id("inputfirstname")).sendKeys(firstname);
		        driver.findElement(By.id("inputlastname")).sendKeys(lastname);
		        driver.findElement(By.id("inputphone")).sendKeys(phone);
		        driver.findElement(By.id("inputemail")).sendKeys(email);
		        driver.findElement(By.id("inputpassword")).sendKeys(password);
		        driver.findElement(By.id("inputconfpassword")).sendKeys(confirmpassword);
		        JavascriptExecutor js = (JavascriptExecutor) driver;
		        js.executeScript("window.scrollBy(0,500)");
		        Thread.sleep(1000);
		        driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/form/center/button")).click();
		        Thread.sleep(1000);
		        driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/form/div[7]/div/div/div[2]/button")).click();
		        Thread.sleep(1000);
				String ActualTitle = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/ul/li")).getText();
				String ExpectedTitle = sheet.getRow(i).getCell(8).toString();
				Row rows = sheet.getRow(i);
				Cell cell_1 = rows.createCell(1);
				cell_1.setCellValue(test_Title);
				Cell cell_2 = rows.createCell(9);
				cell_2.setCellValue(ActualTitle);
				if (ActualTitle.equals(ExpectedTitle)) {
					Cell cell_3 = rows.createCell(10);
					cell_3.setCellValue("Passed");
		            System.out.println("Step 1 : passed.");
				} else {
					Cell cell_3 = rows.createCell(10);
					cell_3.setCellValue("Passed");
		            System.out.println("Step 1 : failed!");
				}
				Cell cell_4 = rows.createCell(11);
				cell_4.setCellValue(test_Date);
				Cell cell_5 = rows.createCell(12);
				cell_5.setCellValue(tester_Name);
				FileOutputStream fos = new FileOutputStream(excel_Path);
				workbook.write(fos);
				System.out.println("Add value to Excel is complete.");
				
		        Thread.sleep(3000);
				File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		        try {
		            FileUtils.copyFile(screenshot, new File("C:/Users/Aspire V/eclipse-workspace/TUC01_Reservation/images_test/"+Test_ID+".png"));
		            System.out.println("Upload image file is complete.");
		        } catch (IOException e) {
		            System.out.println(e.getMessage());
		        }
		        System.out.println("Test "+Test_ID+" Successfully. ^^");
			} else {
				System.out.println("Runing... "+Test_ID);
				String firstname = sheet.getRow(i).getCell(2).toString();
				String lastname = sheet.getRow(i).getCell(3).toString();
				String phone;
				if (Test_ID.equals("TUC01_02")) {
					phone = "";
				} else {
					phone = sheet.getRow(i).getCell(4).toString();
				}
				String email = sheet.getRow(i).getCell(5).toString();
				String password = sheet.getRow(i).getCell(6).toString();
				String confirmpassword = sheet.getRow(i).getCell(7).toString();
		        driver.findElement(By.xpath("/html/body/nav/div/div/center/a[2]")).click();
		        driver.findElement(By.id("inputfirstname")).sendKeys(firstname);
		        driver.findElement(By.id("inputlastname")).sendKeys(lastname);
				driver.findElement(By.id("inputphone")).sendKeys(phone);
		        driver.findElement(By.id("inputemail")).sendKeys(email);
		        driver.findElement(By.id("inputpassword")).sendKeys(password);
		        driver.findElement(By.id("inputconfpassword")).sendKeys(confirmpassword);
		        driver.findElement(By.id("cb-acceptrule")).click();
		        JavascriptExecutor js = (JavascriptExecutor) driver;
		        js.executeScript("window.scrollBy(0,500)");
		        Thread.sleep(1000);
		        driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/form/center/button")).click();
		        Thread.sleep(1000);
		        driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/form/div[7]/div/div/div[2]/button")).click();
		        Thread.sleep(1000);
		        String ActualTitle;
		        if (Test_ID.equals("TUC01_06")) {
		        	ActualTitle = driver.findElement(By.xpath("/html/body/div[3]/div/div[3]")).getText();
		        } else {
					ActualTitle = driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/ul/li")).getText();
		        }
				String ExpectedTitle = sheet.getRow(i).getCell(8).toString();
				Row rows = sheet.getRow(i);
				Cell cell_1 = rows.createCell(1);
				cell_1.setCellValue(test_Title);
				Cell cell_2 = rows.createCell(9);
				cell_2.setCellValue(ActualTitle);
				if (ActualTitle.equals(ExpectedTitle)) {
					Cell cell_3 = rows.createCell(10);
					cell_3.setCellValue("Passed");
		            System.out.println("Step 1 : passed.");
				} else {
					Cell cell_3 = rows.createCell(10);
					cell_3.setCellValue("Passed");
		            System.out.println("Step 1 : failed!");
				}
				Cell cell_4 = rows.createCell(11);
				cell_4.setCellValue(test_Date);
				Cell cell_5 = rows.createCell(12);
				cell_5.setCellValue(tester_Name);
				FileOutputStream fos = new FileOutputStream(excel_Path);
				workbook.write(fos);
				System.out.println("Add value to Excel is complete.");
				
		        Thread.sleep(3000);
				File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		        try {
		            FileUtils.copyFile(screenshot, new File("C:/Users/Aspire V/eclipse-workspace/TUC01_Reservation/images_test/"+Test_ID+".png"));
		            System.out.println("Upload image file is complete.");
		        } catch (IOException e) {
		            System.out.println(e.getMessage());
		        }
		        System.out.println("Test "+Test_ID+" Successfully. ^^");
			}
	        driver.close();
//			workbook.close();
	    }
		System.out.println("==================================================");
        System.out.println("Test "+test_Title+" ("+testCase_ID+")");
        System.out.println("By "+tester_Name);
        System.out.println("On "+test_Date);
        System.out.println("Test Successfully.");
		System.out.println("==================================================");
    }
}